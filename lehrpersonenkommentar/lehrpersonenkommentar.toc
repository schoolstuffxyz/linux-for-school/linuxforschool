\contentsline {section}{Die Website}{3}{section*.3}%
\contentsline {section}{Die verschiedenen Geschmäcker von Linux}{3}{section*.4}%
\contentsline {section}{Die Unterrichtseinheiten}{4}{section*.6}%
\contentsline {subsection}{0 | Linux installieren}{4}{section*.7}%
\contentsline {subsubsection}{Standardmethode}{4}{section*.8}%
\contentsline {subsubsection}{Lernstick}{5}{section*.9}%
\contentsline {subsection}{1 | Grundlagen}{6}{section*.11}%
\contentsline {subsubsection}{Programme}{6}{section*.12}%
\contentsline {subsubsection}{Programme installieren}{6}{section*.16}%
\contentsline {subsubsection}{Terminal}{6}{section*.17}%
\contentsline {subsection}{2 | Die Befehlszeile}{7}{section*.18}%
\contentsline {subsubsection}{Die wichtigsten Befehle}{7}{section*.19}%
\contentsline {subsubsection}{Bashcrawl}{7}{section*.20}%
\contentsline {subsubsection}{Shell-Skripte}{8}{section*.21}%
\contentsline {subsection}{3 | Mein Webserver}{9}{section*.22}%
\contentsline {subsubsection}{HTML}{9}{section*.23}%
\contentsline {subsubsection}{CSS}{9}{section*.24}%
\contentsline {subsubsection}{Der Server}{10}{section*.25}%
\contentsline {subsubsection}{Raspberry Pi}{10}{section*.26}%
